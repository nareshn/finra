const jsonfile = require('jsonfile');
const writeFile = require('write');
const glob = require('glob');
const _ = require('lodash');

let out = [];

const files = glob.sync(`../data/batch/**.json`);
files.forEach((file, index) => {
    console.log(file);
    data = jsonfile.readFileSync(file);
    data = data.hits.hits.reduce((data, emp) => {
        emp = emp._source;
        const t = [];
        t.push(emp.ind_source_id);
        t.push(emp.ind_firstname);
        t.push(emp.ind_middlename);
        t.push(emp.ind_lastname);
        if (emp.ind_current_employments.length) {
            t.push(emp.ind_current_employments[0].firm_id || "")
            t.push(emp.ind_current_employments[0].firm_name || "");
        }
        data.push(t.join("|"));
        return data;
    }, []);

    out = out.concat(data);
});

console.log(out.length);
out = _.uniq(out);
console.log(out.length);

writeFile.sync('../data/summary.txt', out.join("\r\n"), {
    flag: "w"
});Arjun