const jsonfile = require('jsonfile');
const glob = require("glob");
const writeFile = require('write');
const _ = require('lodash');

let out = [];


const files = glob.sync(`../data/individual/**.json`);

files.forEach((file, index) => {
    console.log(index);
    data = jsonfile.readFileSync(file);
    data = JSON.parse(data.hits.hits[0]._source.content);
    info = data.basicInformation;
    let c = data.currentEmployments ? data.currentEmployments[0] : {};
    c= c || {};
   out.push([info.individualId, info.firstName, info.middleName, info.lastName,c.firmId,c.firmName,info.otherNames.join(",")].join("|"));

});

writeFile.sync('../data/summary.txt', out.join("\r\n"), {
    flag: "w"
});
