const request = require('request');
const jsonfile = require('jsonfile');
const fs = require('fs')


const path = "../data/individual";

fs.existsSync(path) || fs.mkdirSync(path, {
    recursive: true
});

const crds = jsonfile.readFileSync('../data/crds.json');

function save(crd, obj) {
    jsonfile.writeFile(path + '/' + crd + '.json', obj, function (err) {
        if (err) console.error(err)
    })
}


function getData(crd) {
    if (!crds.length) {
        return;
    }

    console.log("Left : " + crds.length);
    if (fs.existsSync(path + '/' + crd + '.json')) {
        console.log(crd + " - exits ");
        setTimeout(() => {
            getData(crds.shift());
        });
        return;
    }

    let url = `https://api.brokercheck.finra.org/individual/${crd}`;

    request(url, (error, response, body) => {
        console.log(crd + " - start");
        if (error) {
            console.log(err);
            console.log(crd + " failed");
            return;
        }
        body = JSON.parse(body);
        save(crd, body);
        setTimeout(() => {
            getData(crds.shift());
        }, 1000);
    });
}

getData(crds.shift());