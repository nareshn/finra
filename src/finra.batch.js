const request = require('request');
const jsonfile = require('jsonfile');
const fs = require('fs');

const path = '../data/batch';
const state = 'NY';
const city = 'NEW YORK';
const firms = ['barclays','BNP PARIBAS','CITIGROUP','CREDIT SUISSE','DEUTSCHE BANK','STANDARD CHARTERED','ubs','WELLS FARGO',
                'GOLDMAN SACHS','HSBC','MERRILL LYNCH', 'jp morgan', 'morgan stanley'];


fs.existsSync(path) || fs.mkdirSync(path, { recursive: true });


function getData(start, eStart, eEnd,fileIndex) {

    const url = `http://api.brokercheck.finra.org/individual?city=${city}&filter=broker%3Dtrue,ia%3Dtrue,brokeria%3Dtrue,active%3Dtrue,prev%3Dtrue,bar%3Dtrue,experience=${eStart}-${eEnd}&hl=true&includePrevious=true&nrows=100&r=25&sort=score+desc&start=${start}&state=${state}&wt=json`;


    request(url, (error, response, body) => {

        console.log(start   + "  - start | exp - " + eStart + " ");

        if (error) {
            console.log(err);
            console.log(start + " - failed");
            return;
        }

        body = JSON.parse(body);
        save(fileIndex, body);
        console.log(body.hits.total + " - Retrieved");
        
        if(eEnd < 20000) {
            return;
        }

        if (start > body.hits.total) {
            if (body.hits.total) {
                setTimeout(() => {
                    getData(0, eStart+100, eEnd+100, fileIndex+1);
                }, 0);
            }
            return;
        }

        setTimeout(() => {
            getData(start + 100, eStart, eEnd, fileIndex+1);
        }, 0);
    });
}


function save(fileIndex, data) {
    jsonfile.writeFile(`${path}/s-` + fileIndex + '.json', data, function (err) {
        if (err) console.error(err)
    })
}


getData(0,0,100,0);