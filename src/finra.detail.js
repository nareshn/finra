const jsonfile = require('jsonfile');
const glob = require("glob");
const writeFile = require('write');
const _ = require('lodash');

const out = [];


let files = glob.sync(`../data/individual/**.json`);

files.forEach((file, index) => {
    data = jsonfile.readFileSync(file);
    console.log(index + " " + file);
    
    data = JSON.parse(data.hits.hits[0]._source.content);
    individualId = data.basicInformation.individualId;
    let pEmp = data.previousEmployments;
    let se = data.stateExamCategory;
    let pie = data.principalExamCategory;
    let pre = data.productExamCategory

    pEmp = (pEmp || []).reduce((acc, item) => {
        acc.push(item.firmName);
        return acc;
    }, []).join(";");

    se = (se || []).reduce((acc, item) => {
        acc.push(item.examCategory);
        return acc;
    }, []).join(";");

    pie = (pie || []).reduce((acc, item) => {
        acc.push(item.examCategory);
        return acc;
    }, []).join(";");

    pre = (pre || []).reduce((acc, item) => {
        acc.push(item.examCategory);
        return acc;
    }, []).join(";");

   out.push([individualId,[se,pie,pre].filter(a=>a.length).join(";"),pEmp].join("|"));

});

writeFile.sync('../data/detail.txt', out.join("\r\n"), {
    flag: "w"
});
