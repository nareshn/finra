const puppeteer = require('puppeteer');
var $ = require("jquery");
const fs = require('fs');
const jsonfile = require('jsonfile');

const path = '../data/iapd';
fs.existsSync(path) || fs.mkdirSync(path, {
    recursive: true
});


let url = 'https://adviserinfo.sec.gov/IAPD/IAPDSearch.aspx';

(async () => {
    let index = 2994;
    const browser = await puppeteer.launch({
        headless: true
    });
    const page = await browser.newPage();
    await page.setViewport({
        width: 1920,
        height: 926
    });
    await page.goto(url);

    await page.evaluate(() => {
        $('#ctl00_cphMain_sbox_ddlZipRange').val("25");
        $('#ctl00_cphMain_sbox_txtZip').val("10109");
        $("#ctl00_cphMain_sbox_searchBtn").click();
    });


    while (index < 6593) {

        await page.waitForNavigation({
            timeout: 3000000,
            waitUntil: 'domcontentloaded'
        });

        console.log(index);

        try {
        let out = await page.evaluate(() => {
            let acc = [];
            $('.bcsearchresultfirstcol').each(function () {
                let item = {};
                item.crd = $(this).find('.displaycrd').text();
                item.name  = $(this).find('.displayname').text();
                const spans = $(this).find("div span").toArray().slice(0, 2);
                if ($(spans[0]).attr("id")) {
                    item.aName = $(spans[0]).text();
                    item.company = $(spans[1]).text();
                } else {
                    item.company = $(spans[0]).text();
                }
                acc.push(item);
            });
            return acc;
        });

        jsonfile.writeFile('../data/iapd/iapd.summary-s' + index + '.json', out, function (err) {
            if (err) console.error(err)
        });


    }catch(e) {
        console.log("Error" + index);
    }finally {
        index = index + 1;
 
        await page.evaluate((index) => {
            WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions("ctl00$cphMain$ucSearchPagerTop$rptPager$ctl$09lkbPageNo", "", true, "", "", false, true));
         },index);
    }
}

})();