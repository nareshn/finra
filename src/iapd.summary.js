const jsonfile = require('jsonfile');
const glob = require("glob");
const writeFile = require('write');
const _ = require('lodash');

let out = {};


const files = glob.sync(`../data/iapd/**.json`);

files.forEach((file, index) => {
    data = jsonfile.readFileSync(file);
    data.forEach(info=>{
        info.individualId = info.crd.replace("CRD#","").replace("(","").replace(")","").trim();
        let name = info.name.trim().split(" ");
        info.firstName = (name[0]||"").trim();info.lastName = (name[2]||"").trim();info.middleName = (name[1]||"").trim();
        info.firmName = info.company.split("(CRD#")[0].trim();
        info.firmId = info.company.split("(CRD#")[1].replace(")","").trim();
        out[info.individualId] = [info.individualId, info.firstName, info.middleName, info.lastName,info.firmId,info.firmName,info.aName].join("|");
    });

});


writeFile.sync('../data/iapd.summary.txt', Object.values(out).join("\r\n"), {
    flag: "w"
});
