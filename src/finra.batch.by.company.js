const request = require('request');
const jsonfile = require('jsonfile');
const fs = require('fs');

const path = '../data/batch';


fs.existsSync(path) || fs.mkdirSync(path, {
    recursive: true
});


const firms = jsonfile.readFileSync('../data/firms.json');

const MaxExp = 21900;


function getData(start, eStart, eEnd, fileIndex, firm) {


    const url = `https://api.brokercheck.finra.org/individual?filter=broker%3Dtrue,ia%3Dtrue,brokeria%3Dtrue,active%3Dtrue,prev%3Dtrue,bar%3Dtrue,experience=${eStart}-${eEnd}&firm=${firm}&hl=true&includePrevious=true&nrows=100&r=25&sort=score+desc&start=${start}&wt=json`;

    request(url, (error, response, body) => {


        if (error) {
            console.log(`start:${start} | exp:${eStart}-${eEnd} | firm:${firm}`);
            console.log(err);
            return;
        }

        body = JSON.parse(body);
        if(body.hits.total && body.hits.hits.length) {
            save(body, start, eStart, eEnd, fileIndex, firm);
        }
        console.log(`start:${start} | exp:${eStart}-${eEnd} | firm:${firm} | total: ${body.hits.total}`);

        if (eEnd >= MaxExp && !firms.length) {
            return;
        }

        if (start >= body.hits.total) {
            if (eEnd >= MaxExp && firms.length) {
                firm = firms.pop();
                eStart = -200; eEnd = 0;
            }
            setTimeout(() => {
                getTotal(firm, (total) => {
                    console.log("Total : " + total + " | firms: " + firms.length);
                    if (total > 9000) {
                        getData(0, eStart + 200, eEnd + 200, fileIndex + 1, firm);
                    } else {
                        getData(0, 0, MaxExp, fileIndex + 1, firm);
                    }
                });

            }, 0);
            return;
        }

        setTimeout(() => {
            getData(start + 100, eStart, eEnd, fileIndex + 1, firm);
        }, 0);
    });
}


function getTotal(firm, cb) {
    const url = `https://api.brokercheck.finra.org/individual?firm=${firm}&hl=true&includePrevious=true&nrows=12&r=25&sort=score+desc&wt=json`;
    request(url, (error, response, body) => {

        body = JSON.parse(body);
        cb(body.hits.total);

    });

}

function save(data, start, eStart, eEnd, fileIndex, firm) {
    const name = `${firm}-${start}-${eStart}-${eEnd}-${fileIndex}`;
    jsonfile.writeFile(`${path}/` + name + '.json', data, function (err) {
        if (err) console.error(err)
    })
}


getData(0, 0, 200, 0, firms.pop());


//47040