const request = require('request');
const jsonfile = require('jsonfile');
const fs = require('fs');

const path = '../data/batch.company';

const states = ['AL', 'AK', 'AS', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FM', 'FL', 'GA',
    'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MH', 'MD', 'MA',
    'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND',
    'MP', 'OH', 'OK', 'OR', 'PW', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT',
    'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'WY'
];

fs.existsSync(path) || fs.mkdirSync(path, {
    recursive: true
});


function getData(start, state, fileIndex) {

    const url = `https://api.brokercheck.finra.org/firm?hl=true&nrows=100&r=25&sort=score+desc&state=${state}&wt=json&&start=${start}`;

    request(url, (error, response, body) => {

        console.log(start + "  - start | state - " + state + " ");

        if (error) {
            console.log(err);
            console.log(start + " - failed");
            return;
        }

        body = JSON.parse(body);
        save(fileIndex, body);
        console.log(body.hits.total + " - Retrieved");

        if (start < body.hits.total) {
            setTimeout(() => {
                getData(start + 100, state, fileIndex + 1);
            }, 0);
            return;
        }

        setTimeout(() => {
            getData(0, states.pop(), fileIndex + 1);
        }, 0);
    });
}


function save(fileIndex, data) {
    jsonfile.writeFile(`${path}/s-` + fileIndex + '.json', data, function (err) {
        if (err) console.error(err)
    })
}


getData(0, states.pop(), 0);