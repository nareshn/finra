const jsonfile = require('jsonfile');
const writeFile = require('write');
const glob = require('glob');
const _ = require('lodash');

let out = [];

const files = glob.sync(`../data/batch/**.json`);
files.forEach((file, index) => {
    data = jsonfile.readFileSync(file);
    data = data.hits.hits.reduce((data, emp) => {
        emp = emp._source;
        company = emp.ind_current_employments.reduce((acc, s) => {
            if (isValid(s.firm_id)) {
                acc.push(s.firm_id);
                acc.push(s.firm_name);
            }
            return acc;
        }, []);
        company = _.uniq(company);
        if (company.length) {
            data.push(emp.ind_source_id);
        }
        return data;
    }, []);

    console.log(out.length);
});

out = _.uniq(out);
console.log(out.length);

writeFile.sync('../data/crds.json', '[' + out.join(",") + ']', {
    flag: "w"
});

function isValid(id) {
    // return [8209, 79, 361, 7691, 19714, 816, 2525, 7654, 8174].find(a => a == id);
    return true;
}