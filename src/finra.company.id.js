const jsonfile = require('jsonfile');
const writeFile = require('write');
const glob = require('glob');
const _ = require('lodash');

let out = [];

const files = glob.sync(`../data/batch.company/**.json`);
files.forEach((file, index) => {
    data = jsonfile.readFileSync(file);
    data = data.hits.hits.reduce((data, json) => {
        //let address = json._source.firm_address_details || json._source.firm_ia_address_details;
        //let state = address ? JSON.parse(address).officeAddress.state : "";
        //data.push(json._source.firm_source_id+"|"+state+"|"+json._source.firm_name);
        data.push(json._source.firm_source_id);
        return data;
    }, []);
    out = out.concat(data);
});

out = _.uniq(out);
console.log(out.length);

writeFile.sync('../data/firms.json', "[" + out.join(",") + "]", {
    flag: "w"
});